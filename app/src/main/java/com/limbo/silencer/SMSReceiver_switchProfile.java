package com.limbo.silencer;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by Limbo on 7/26/2017.
 */

public class SMSReceiver_switchProfile extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "SMS Received: " + intent.getAction().toString(),
                Toast.LENGTH_LONG).show();

        if(intent.getAction().equalsIgnoreCase("android.provider.Telephony.SMS_Received")){
            Bundle bundle = intent.getExtras();
            SmsMessage[] chunks=null;
            String smsBody="";
            if(bundle!=null){
                Object[] pdus= (Object[]) bundle.get("pdus");
                chunks=new SmsMessage[pdus.length];
                for(int i=0;i<pdus.length;i++){
                    chunks[i]=SmsMessage.createFromPdu((byte[])pdus[i]);
                    smsBody += chunks[i].getMessageBody().toString();
                }

                //for specific message "Find my phone" phone will switch to normal mode
                if (smsBody.contains("Find my phone"))
                {
                    Intent intent1 = new Intent(context, MainActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 12345, intent1, PendingIntent.FLAG_CANCEL_CURRENT);
                    AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                }

            }
        }

    }
}
